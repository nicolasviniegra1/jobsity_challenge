--ONCE YOU HAVE CREATED THE LOCAL CONNECTION, RUN THESE COMMANDS TO CREATE THE DATABASE AND THE TABLE
create database trips_database

use trips_database

create table trips (
	id int not null auto_increment,
    region varchar(50) not null,
    origin_coord_x double not null,
    origin_coord_y double not null,
    destination_coord_x double not null,
    destination_coord_y double not null,
    date date not null,
    time time not null,
    datasource varchar(50) not null,
    primary key (id)
);