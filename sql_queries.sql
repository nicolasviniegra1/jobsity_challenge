--SELECT THE LATEST DATASOURCE FROM THE TWO MOST COMMONLY APPEARING REGIONS (LATEST DATASOURCE FOR EACH REGION)
WITH tr AS (
   SELECT region, datasource
        , row_number() OVER(PARTITION BY region ORDER BY date DESC, time) AS rn
   FROM   trips
   where region in ( select * from (
	select region
	from trips
	group by region
	order by count(region) DESC
	limit 2 ) as trips2
    )
   )
SELECT region, datasource
FROM   tr
WHERE  rn = 1;

--SELECT THE LATEST DATASOURCE FROM THE TWO MOST COMMONLY APPEARING REGIONS (LATEST DATASOURCE FOR BOTH REGIONS)
WITH tr AS (
   SELECT region, datasource, date, time
        , row_number() OVER(PARTITION BY region ORDER BY date DESC, time) AS rn
   FROM   trips
   where region in ( select * from (
	select region
	from trips
	group by region
	order by count(region) DESC
	limit 2 ) as trips2
    )
   )
SELECT region, datasource
FROM   tr
WHERE  rn = 1
ORDER BY date DESC, time
LIMIT 1;

--SELECT THE REGIONS THAT HAVE CHEAP_MOBILE AS DATASOURCE IN AT LEAST ONE ROW
select distinct region
from trips 
where datasource in ('cheap_mobile');