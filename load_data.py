from dask.distributed import Client, LocalCluster, progress
import dask.dataframe as dd
import pymysql

#apply transformations to the data
def apply_transformations_and_load(df):

    #split the origin coords in two fields, containing one the x coord and another the y coord
    df['origin_coord'] = df['origin_coord'].apply(lambda x: x.replace('POINT (', '').replace(')', ''), meta = ('origin_coord', 'str'))
    df[['origin_coord_x', 'origin_coord_y']] = df['origin_coord'].str.split(' ', 1, expand = True)

    #split the destination coord in two fields, containing one the x coord and another the y coord
    df['destination_coord'] = df['destination_coord'].apply(lambda x: x.replace('POINT (', '').replace(')', ''), meta = ('origin_coord', 'str'))
    df[['destination_coord_x', 'destination_coord_y']] = df['destination_coord'].str.split(' ', 1, expand = True)

    #split the datetime field in two fields, date and time
    df[['date', 'time']] = df['datetime'].str.split(' ', 1, expand = True)

    #select the fields I'll use to store on the database
    transformed_df = df[['region', 'origin_coord_x', 'origin_coord_y', 'destination_coord_x', 'destination_coord_y', 'date', 'time', 'datasource']]

    #transform the dask dataframe into a pandas dataframe to use pandas functions
    transformed_df = transformed_df.compute()

    #convert dataframe to list of lists
    list_values = transformed_df.values.tolist()

    #create the connection to the db
    host = '127.0.0.1'
    user = 'root'
    database = 'trips_database'
    password = 'asdasd123'

    connection = pymysql.connect(host = host, port = 3306, user = user, database = database, password = password)
    with connection:
        cur = connection.cursor()
        cur.execute("SELECT VERSION()")
        version = cur.fetchone()


    #insert the values into the table
    connection.ping()  # reconnecting mysql
    with connection.cursor() as cursor:
        cursor.executemany("insert into trips(region, origin_coord_x, origin_coord_y, destination_coord_x, destination_coord_y, date, time, datasource) values (%s, %s, %s, %s, %s, %s, %s, %s)", list_values)
        connection.commit()


    return transformed_df


if __name__ == '__main__':
    #create local cluster for distributed processing in local mode
    cluster = LocalCluster()
    #create client to process the data
    client = Client(cluster)

    #load csv
    csv = ''

    while csv == '':
        csv = input("Enter the path for the csv file: ")
        try:
            df = dd.read_csv(csv)
        except:
            print("Csv not foung with the specified path")
            csv = ''

    #process, apply transformations and store in a distributed way
    df = client.persist(df)

    partitions = [part for part in df.partitions]

    futures = client.map(apply_transformations_and_load, partitions)
    progress(futures)



