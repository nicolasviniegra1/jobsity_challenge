Name
Jobsity Challenge

Description
This project can store data from a csv into a relational database, it also can do some calculations and provides queries to analyze the stored data

Installation
You must have Python and pip installed
Clone the repository into your local computer, open a terminal and move the the project directory.
Run the following command: pip install -r requirements.txt to install the dependencies

After that, create a local Mysql connection with the following credentials:

    host = 127.0.0.1
    user = root
    password = asdasd123

!!! To run one of the queries in the "sql_queries.sql" file you must use the 8.0 version of Mysql Server !!!

Usage
First of all, you must create the database and table in your Mysql local connection.
Once you have created the connection, use the commands that are listed in the file "sql commands.sql", run those commands on an sql editor with the Mysql connection established

After that, you have to run the "load_data.py" file to store the data of the csv file in the database.
On a terminal, run the command "python load_data.py", it will show you the process of the ingestion

After the data is stored, you can run the "obtain_weekly_average.py" file to obtain the weekly average number of trips for a specific area.
You can select the area in two ways.

    1 - By bounding box, you must specify the values of the sides of a box that will be used to get all the trips that were made inside those values, and after that will be used to calculate the average.

    2 - By region, you must specify the region of the trips.

Also, you can run the queries that are written in the "sql_queries.sql" file, to analize the stored data


Considerations:

First consideration:

    I didn't understand exactly the first query of the "sql_queries.sql" file.
    The statement is: "From the two most commonly appearing regions, which is the latest datasource?"
    I didn't understand if you want to get the latest datasource for each of the two most commonly appearing region, or from both regions, que the latest datasource. 
    I wrote the queries for both possibilities.

Second consideration:

    To run the queries of the previous consideration, you must use Mysql Server v8.0

Third consideration:

    Scalable solution: Here I'll explain in detail how the solution is scalable.
    The "load_data.py" file uses a library called "Dask". This library helps processing dataframes in a distributed way. For large amounts of data, it's better than pandas.
    You can read more about dask in this link: https://dask.org/

    In this solution, I create a local cluster, since I don't have a remote processing server or unit. With my current computer, it won't run in a distributed way because I don't have any other computer connected, so all the processing will be done by one computer, and also because the csv is small enough to be processed all together.
    But if the csv becomes larger, let's say, 10 millions records, or 100 millions records, the solution is ready to process that amount of data in a distributed way. You will have to add more computers so that you can increase your processing power, but that already has to do with the hardware. When it comes to software, the solution is scalable.
    When Dask reads a csv and converts it to a dataframe, it automatically partitions that dataframe based on its size. Those partitions can be processed distributedly with a cluster. Dask will assign each partition to a processing unit in the cluster. The more processing units the cluster has, the more parallelized will be the processing.
    So, in other words, the current limit now is the hardware, the software has the tools to be scalable.

    More resources:

    https://distributed.dask.org/en/stable/

Fourth consideration:

    Bounding box for obtaining the weekly average number of trips:
    Here the method I thought was to takes as input the coordinates of the sides of the box.

           top
        ----------
        |  x     |
    left|      x |right
        |  x     |
        ----------
          bottom

    You will have to enter four values, the values left and right for the x-axis and the top and bottom for the y-axis. So after entering those values. The sql query will search for records that have the x coordinate between the left and right values, and also the y coordinate must be betwwen the top and right values.


Fifth consideration:

    The only feature I couldn't solve was the one asking for grouping together trips with similar origin, destination, and time. I didn't get to understand correctly what did the statement mean with grouping them.

Sixth consideration:

    Dask already satisfies the feature of informing the user about the status of the data ingestion by printing the status in the terminal and updating it.

Seventh consideration:

    Obtain weekly average of trips.

    The solution I developed was the following:
    After getting the trips for the corresponding area, I count how many trips belong to each week and year.
    So, after doing this, I'll have a dictionary with all the existing week-year pairs and how many trips belong to each.
    For example it could be:

    {year-week_of_year}
    2018-03 -> 3 trips
    2018-04 -> 7 trips
    2018-05 -> 4 trips
    ...

    After doing this, now I sum all the counts of trips, and divide that sum by the number of {year-week_of_year} pairs, this will give me the weekly average number of trips.


Other considerations:

    I didn't containerize the solution. I haven't worked with Docker or any othe containers.

    In the email I sent you, you will see a word file attached that explains how would I set up the solution on AWS.