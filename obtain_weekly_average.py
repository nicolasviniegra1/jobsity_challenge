
import pymysql

#create connection
def connect():
    host = '127.0.0.1'
    user = 'root'
    database = 'trips_database'
    password = 'asdasd123'

    connection = pymysql.connect(host = host, port = 3306, user = user, database = database, password = password)
    with connection:
        cur = connection.cursor()
        cur.execute("SELECT VERSION()")
        version = cur.fetchone()

    return connection



def get_data(values):

    connection = connect()

    connection.ping()
    #if values contains only one element, it means the value is the region, 
    #if not, the elements are the coordinates of the bounding box
    if len(values) == 1:
        
        region = values[0]
        sql = "SELECT date FROM trips WHERE region ='%s'"

        with connection.cursor() as cursor:         
            cursor.execute(sql % region)        
    else:
        
        coord_top = values[0] #top
        coord_bottom = values[1] #bottom
        coord_left = values[2] #left
        coord_right = values[3] #right

        #Here I'm taking into account trips that have destination or origin inside the bounding box
        sql = "SELECT date FROM trips WHERE (origin_coord_x >= '%s' and origin_coord_x <= '%s' and \
                                                 origin_coord_y <= '%s' and origin_coord_y >= '%s') or \
                                                (destination_coord_x >= '%s' and destination_coord_x <= '%s' and \
                                                 destination_coord_y <= '%s' and destination_coord_y >= '%s')"

        with connection.cursor() as cursor:
            cursor.execute(sql % (coord_left, coord_right, coord_top, coord_bottom, coord_left, coord_right, coord_top, coord_bottom,))

    year_week_pairs = dict()
    #fill the dictionary with keys and values
    #the keys are a pair of two values, the year, and the number of week of the year
    #the values are the count of rows that each key has
    #with this, I can know how many trips each week has
    for row in cursor:

        #create the pair
        pair_year_week = '{}-{}'.format(row[0].year, row[0].isocalendar()[1])

        #if it doesnt exists I initialize it
        if not year_week_pairs.get(pair_year_week):
            year_week_pairs[pair_year_week] = 0
        #add 1 to the count
        year_week_pairs[pair_year_week] += 1

    #if I dont receive any rows from the query, I return -1 to print a message 
    #indicating that no rows were found for the area specified
    if len(year_week_pairs) == 0:
        return -1
    
    sum_weekly_trips = 0
    #I sum all the counts of trips and then divide by the number of pairs year-week, to get the average
    for pair in year_week_pairs:

        sum_weekly_trips += year_week_pairs[pair]
    
    average_weekly_trips = sum_weekly_trips // len(year_week_pairs)

    return average_weekly_trips

    


if __name__ == '__main__':

    area = ''
    #input type of area and its values
    while area != '2' and area != '1':
        print("Please enter the number for the area type:")
        print("1 - Bounding box")
        print("2 - Region")
        area = input("Area type: ")

    if area == '1':
        print("Enter the 4 coordinates of the bounding box")
        coord_1 = coord_2 = coord_3 = coord_4 = ''
        #receive inputs and validate them
        while type(coord_1) != float:
            coord_1 = input("Coordinate 1 top: ")
            try:
                coord_1 = float(coord_1)
            except:
                print("Enter a valid value")
        while type(coord_2) != float:
            coord_2 = input("Coordinate 2 bottom: ")
            try:
                coord_2 = float(coord_2)
            except:
                print("Enter a valid value")
        while type(coord_3) != float:
            coord_3 = input("Coordinate 3 left: ")
            try:
                coord_3 = float(coord_3)
            except:
                print("Enter a valid value")
        while type(coord_4) != float:
            coord_4 = input("Coordinate 4 right: ")
            try:
                coord_4 = float(coord_4)
            except:
                print("Enter a valid value")

        values = [coord_1, coord_2, coord_3, coord_4]
    else:
        print("Enter the region")
        region = input("Region: ")

        values = [region]

    #calculate average
    average = get_data(values)

    if average == -1:
        print("No records were found for the area indicated")
    else:
        print("The average number of trips for the area mentioned is: {}".format(average))



    